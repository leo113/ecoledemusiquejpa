package metier;

import java.util.ArrayList;

import modele.beans.Personne;

/**
 * Classe pour la gestion de la liste des adhérents
 * Genere un nouvel identifiant pour chaque nouvel objet de type Personne
 *  ajouté a la liste, et met l'objet de type Personne a jour avec 
 *  cet identifiant.
 * @author Fanny
 *
 */
public class ListeAdherents {
	
	private ArrayList<Personne> listPersonne = new ArrayList<Personne>();
	
	//private static int nbrAdherents;

	// GETTERS ET SETTERS
	public ArrayList<Personne> getListPersonne() {
		return listPersonne;
	}
	public void setListPersonne(ArrayList<Personne> listPersonne) {
		this.listPersonne = listPersonne;
	}	
	
	// METHODE ADD
	public ArrayList<Personne> add(Personne personne) {
		listPersonne.add(personne);		
		//personne.setId(++nbrAdherents);
		return this.listPersonne;
	}
	
	// METHODE REMOVE
	public ArrayList<Personne> remove(Personne personne) {
		listPersonne.remove(personne);
		return this.listPersonne;
	}
}
