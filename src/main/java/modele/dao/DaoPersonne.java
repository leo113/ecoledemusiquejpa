package modele.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import Exceptions.ExceptionDao;
import modele.beans.Personne;
import servlet.FrontController;


public class DaoPersonne {
	
	// ATTRIBUTS D INSTANCE
	@PersistenceContext(unitName = "ecole_musique")	
	EntityManager em = FrontController.entityManager;
	

	// METHODE SAVE(PERSONNE) :
	/**
	 * Methode qui prend un objet de type Personne en parametre,
	 *  et l'enregistre dans la Bdd. (Tables Personne).
	 * Si la personne existe deja en Bdd, met simplement à jour ses attributs en bdd.
	 * Si la personne n existe pas encore dans la Bdd, alors rajoute une ligne 
	 * 	dans la Bdd pour cette Personne, dans la table 'Personne'.
	 * Retourne l objet Personne si celui ci a ete enregistrer correctement en Bdd.
	 * Lance une ExceptionDao si l objet personne passe en parametre a comme valeur 'null'.
	 * Lance une ExceptionDao si un pbe est rencontre lors de l enregistrement en bdd.
	 * @param personne Personne
	 * @return Personne
	 * @throws ExceptionDao
	 */
	public Personne save(Personne personne) throws ExceptionDao {	
		
		// SI LA PERSONNE VAUT NULL :
		//  ON RENVOI UNE EXCEPTION
		if (personne == null) {
			throw new ExceptionDao("Dao Exception. Methode createPersonne(personne)\n"
									+ "La valeur de l'objet 'personne' est null.");
		}			
	
		// SI LA PERSONNE POSSEDE UN IDENTIFIANT :
		//  - SI LA PERSONNE N EXISTE PAS DEJA EN BDD : 
		//    ALORS ON RETOURNE 'NULL'
		//  - SI LA PERSONNE EXISTE DEJA EN BDD : 
		//    ALORS ON MET A JOUR SES VALEURS EN BDD
	    if (personne.getId()!=null) {
	    	if ( (new DaoPersonne().find(personne.getId()) == null)) {
				return null;
			}
	    	try {
	    		em.getTransaction().begin();
	    		personne = em.merge(personne);
	    		em.flush();
		    	em.persist(personne);
		    	em.getTransaction().commit();
			} catch (Exception e) {
				em.getTransaction().rollback();
				throw new ExceptionDao("Dao Exception1. Methode createPersonne(personne)\n"
						+ e.getMessage());
			}
		// SI LA PERSONNE NE POSSEDE PAS D IDENTIFIANT :
		//  ALORS ON CREER LA PERSONNE EN BDD		
	    }else { // Si la personne n existe pas encore en Bdd, on creer une ligne en Bdd
	    	try {
	    		em.getTransaction().begin();
	    		em.persist(personne);
	    		em.getTransaction().commit();				
			} catch (Exception e) {
				throw new ExceptionDao("Dao Exception1. Methode createPersonne(personne)\n"
						+ e.getMessage());
			}
	    }
		return personne;
	}
	
	// METHODE DELETE(PERSONNE) :
	/**
	 * Methode qui supprime une Personne dans la Bdd 'ecole_musique'.
	 * Renvoit un boleen avec une valeur a True si la suppression est effective en bdd. 
	 * Lance une ExceptionDao si l'identifiant de la personne est negatif ou null
	 *  ou si un pbe est rencontre lors de l enregistrement en bdd.
	 * Prend en parametre l identifiant en bdd de la personne a supprimer en Bdd.
	 * @param  idPersonne Integer
	 * @return Boolean
	 * @throws ExceptionDao
	 */
	public Boolean delete(Integer idPersonne) throws ExceptionDao  {	
		
		// SI L ID_PERSONNE EST NULL OU NEGATIF, ON RENVOI UNE EXCEPTION
		if (idPersonne == null || idPersonne < 0) {
			throw new ExceptionDao("Dao Exception.\nMethode delete.\n"
									+ "Impossible de supprimer une personne "
									+ "avec un IdPersonne 'Null' ( ou un idPersonne avec une valeur negative)" );
		}	
		
		Personne p;
		if ( (p  = ((Personne)  find(idPersonne))) != null ) {
			try {
				em.getTransaction().begin();
				em.remove(p);;
				em.getTransaction().commit();
				return true;				
			} catch (Exception e) {
				throw new ExceptionDao("Dao Exception.\nMethode delete.\n"
						+ e.getMessage() );
			}
		}
		return false;
	}
	
	
	// METHODE FIND(ID_PERSONNE) :
	/**
	 * Recherche l existence d une Personne dans la Bdd 'ecole_musique', d apres son identifiant.
	 * Retourne une instance la personne, si la personne est trouvee en bdd, sinon retourne 'null'.
	 * Prend en parametre l identifiant en bdd de la personne rechercher.
	 * Renvoit un ExceptionDao si l identifiant de la personne est negatif ou null.
	 * @param  int idPersonne
	 * @return Personne personne
	 * @throws ExceptionDao
	 */
	public Personne find(int idPersonne) throws ExceptionDao   {	
		
		if (idPersonne <=0 ) {
			throw new ExceptionDao("Dao Exception. Methode find(personne)\n"
									+ "L idPersonne est negatif.");
		}
		
		Personne p = null;
		p = em.find(Personne.class, idPersonne) ;
		
		return p;
	}
	
	// METHODE FIND_ALL() :
	/**
	 * Recherche la liste des personnes presents dans la Bdd 'ecole_musique'.
	 * Retourne une ArrayList<Personne>.
	 * Retourne une liste vide si la table en Bdd est vide ou si un pbe a été rencontré. 
	 * @return ArrayList<Personne> 
	 */
	public ArrayList<Personne> findAll() {
		
		ArrayList<Personne> listPersonnes = new ArrayList<>();

		List<?> customers =   em.createQuery("SELECT c FROM Personne AS c")
								.getResultList();
		
		// ON CREE LA LISTE RETOURNEE PAR LA METHODE
		for (Object object : customers) {
			Personne p = (Personne)  object;
			listPersonnes.add(p);
		}
		return listPersonnes;
	}
	
	// METHODE FIND_ALL_BY_GROUP() :
	/**
	 * Recherche la liste des personnes presents dans la Bdd 'ecole_musique' pour un 
	 *  groupe en particulier.
	 * Retourne une ArrayList<Personne>.
	 * Retourne une liste vide si la table en Bdd est vide ou si un pbe a été rencontré. 
	 * @return ArrayList<Personne> 
	 */
	public ArrayList<Personne> findAllByGroup(Integer idGroup ) {
		
		ArrayList<Personne> listPersonnes = new ArrayList<>();

		List<?> customers =   em.createQuery("SELECT c FROM Personne AS c where c.groupe.id = ?1")
								.setParameter(1, idGroup)
								.getResultList();
		
		// ON CREE LA LISTE RETOURNEE PAR LA METHODE
		for (Object object : customers) {
			Personne p = (Personne)  object;
			listPersonnes.add(p);
		}
		return listPersonnes;
	}
}