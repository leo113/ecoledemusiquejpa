package modele.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import Exceptions.ExceptionDao;
import modele.beans.Groupe;
import servlet.FrontController;


public class DaoGroupe {
	
	// ATTRIBUTS D INSTANCE
	@PersistenceContext(unitName = "ecole_musique")	
	EntityManager em = FrontController.entityManager;
	

	// METHODE SAVE(GROUPE)
	/**
	 * Methode qui prend un objet de type Groupe en parametre,
	 *  et l'enregistre dans la Bdd. (Tables Groupe).
	 * Si la groupe existe deja en Bdd, met simplement à jour ses attributs en bdd.
	 * Si la groupe n existe pas encore dans la Bdd, alors rajoute une ligne 
	 * 	dans la Bdd pour ce groupe, dans la table 'groupe'.
	 * Retourne l objet groupe si celui ci a ete enregistrer correctement en Bdd.
	 * Lance une ExceptionDao si l objet groupe passe en parametre a comme valeur 'null'.
	 * Lance une ExceptionDao si un pbe est rencontre lors de l enregistrement en bdd.
	 * @param groupe Groupe
	 * @return Groupe
	 * @throws ExceptionDao
	 */
	public Groupe save(Groupe groupe) throws ExceptionDao {	
		
		// SI LE GROUPE VAUT NULL :
		//  ON RENVOI UNE EXCEPTION
		if (groupe == null) {
			throw new ExceptionDao("Dao Exception. Methode createGroupe(groupe)\n"
									+ "La valeur de l'objet 'bgroupe' est null.");
		}			
	
		// SI LE GROUPE POSSEDE UN IDENTIFIANT :
		//  - SI LE GROUPE N EXISTE PAS DEJA EN BDD : 
		//    ALORS ON RETOURNE 'NULL'
		//  - SI LE GROUPE EXISTE DEJA EN BDD : 
		//    ALORS ON MET A JOUR SES VALEURS EN BDD
	    if (groupe.getId()!=null) {
	    	if ( (new DaoGroupe().find(groupe.getId()) == null)) {
				return null;
			}
	    	try {
	    		em.getTransaction().begin();
	    		groupe = em.merge(groupe);
	    		em.flush();
		    	em.persist(groupe);
		    	em.getTransaction().commit();
			} catch (Exception e) {
				em.getTransaction().rollback();
				throw new ExceptionDao("Dao Exception1. Methode createGroupe(groupe)\n"
						+ e.getMessage());
			}
		// SI LE GROUPE NE POSSEDE PAS D IDENTIFIANT :
		//  ALORS ON CREER LE GROUPE EN BDD		
	    }else { // Si la groupe n existe pas encore en Bdd, on creer une ligne en Bdd
	    	try {
	    		em.getTransaction().begin();
	    		em.persist(groupe);
	    		em.getTransaction().commit();				
			} catch (Exception e) {
				throw new ExceptionDao("Dao Exception1. Methode createGroupe(groupe)\n"
						+ e.getMessage());
			}
	    }
		return groupe;
	}
	
	// METHODE DELETE(GROUPE) :
	/**
	 * Methode qui supprime un groupe dans la Bdd 'ecole_musique'.
	 * Renvoit un boleen avec une valeur a True si la suppression est effective en bdd. 
	 * Lance une ExceptionDao si l'identifiant de la groupe est negatif ou null
	 *  ou si un pbe est rencontre lors de l enregistrement en bdd.
	 * Prend en parametre l identifiant en bdd du groupe a supprimer en Bdd.
	 * @param  idGroupe Integer
	 * @return Boolean
	 * @throws ExceptionDao
	 */
	public Boolean delete(Integer idGroupe) throws ExceptionDao  {	
		
		// SI L ID_GROUPE EST NULL OU NEGATIF, ON RENVOI UNE EXCEPTION
		if (idGroupe == null || idGroupe < 0) {
			throw new ExceptionDao("Dao Exception.\nMethode delete.\n"
									+ "Impossible de supprimer un groupe "
									+ "avec un IdGroupe 'Null' ( ou un idGroupe avec une valeur negative)" );
		}	
		
		Groupe p;
		if ( (p  = ((Groupe)  find(idGroupe))) != null ) {
			try {
				em.getTransaction().begin();
				em.remove(p);;
				em.getTransaction().commit();
				return true;				
			} catch (Exception e) {
				throw new ExceptionDao("Dao Exception.\nMethode delete.\n"
						+ e.getMessage() );
			}
		}
		return false;
	}
	
	
	// METHODE FIND(ID_GROUPE) :
	/**
	 * Recherche l existence d'un groupe dans la Bdd 'ecole_musique', d apres son identifiant.
	 * Retourne une instance la groupe, si le groupe est trouvee en bdd, sinon retourne 'null'.
	 * Prend en parametre l identifiant en bdd du groupe rechercher.
	 * Renvoit un ExceptionDao si l identifiant du groupe est negatif ou null.
	 * @param  int idGroupe
	 * @return Groupe groupe
	 * @throws ExceptionDao
	 */
	public Groupe find(int idGroupe) throws ExceptionDao   {	
		
		if (idGroupe <=0 ) {
			throw new ExceptionDao("Dao Exception. Methode find(groupe)\n"
									+ "L idGroupe est negatif.");
		}
		
		Groupe p = null;
		p = em.find(Groupe.class, idGroupe) ;
		
		return p;
	}
	
	// METHODE FIND_ALL() :
	/**
	 * Recherche la liste des groupes presents dans la Bdd 'ecole_musique'.
	 * Retourne une ArrayList<Groupe>.
	 * Retourne une liste vide si la table en Bdd est vide ou si un pbe a été rencontré. 
	 * @return ArrayList<Groupe> 
	 */
	public ArrayList<Groupe> findAll() {
		
		ArrayList<Groupe> listGroupes = new ArrayList<>();

		List<?> customers =   em.createQuery("SELECT c FROM Groupe AS c")
								.getResultList();
		
		// ON CREE LA LISTE RETOURNEE PAR LA METHODE
		for (Object object : customers) {
			Groupe p = (Groupe)  object;
			listGroupes.add(p);
		}
		return listGroupes;
	}
}