package modele.forms;

import javax.servlet.http.HttpServletRequest;

/**
 * Classe qui valide les formulaires de type 'Adherent'
 * @author Fanny
 *
 */
public class PersonneForm {
	
	// ATTRIBUTS D'INSTANCE
	private String classCss = "";
	private String result = "";
	private boolean valid = false;
	
	// GETTERS ET SETTERS
	public String getClassCss() {
		return classCss;
	}
	public String getResult() {
		return result;
	}
	public boolean isValid() {
		return valid;
	}
	
	/**
	 * Vérifit que les champs 'Nom' et 'Prenom' ne sont 
	 *  ni vides ni null.
	 *  Vérifit que le nom et le prenom n ont pas la meme valeur.
	 * Positionne dans ses variables d'instances 'classCss' et 'message'
	 *  des valeurs de classe Css, et une valeur pour le message de success ou d'echec.
	 * @param request
	 */
	public void verifForm(HttpServletRequest request) {
		
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		
		// Positionnement des drapeaux
		if (nom == null || prenom == null ) 
		{
			classCss = "info";
			result =  "Veuillez renseigner tous les champs.";
			valid = false;
		}else {
			if (((nom != null) &&  (prenom != null))  
					&& (! nom.isEmpty())	&& (! prenom.isEmpty())) {
					if (nom.equalsIgnoreCase(prenom)) {
						classCss = "danger";
			    		result =  "Echec. Le Nom et le Prenom ne doivent pas etre identiques.";
			    		valid = false;
					}else {
						classCss = "success";
						result =  "Succès. La liste des adhérents a été mis à jour.";	
						valid = true;
					}				
			}else {
	    		classCss = "danger";
	    		result =  "Echec.Tous les champs doivent etre renseignés.";
	    		valid = false;
			}	
		}	
	}	
}

