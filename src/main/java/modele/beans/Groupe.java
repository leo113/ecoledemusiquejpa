package modele.beans;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

/**
 * Classe gestion des Groupe
 * @author Fanny
 *
 */
@Entity
@Table(name = "groupe")
public class Groupe {
	
	// ATTRIBUTS D'INSTANCE
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name ="id_groupe")
	private Integer id ;


	@Length(min = 5 , message = "La longueur du nom du groupe est de 5 caracteres minimum.")
	@Length(max = 50 , message = "La longueur du nom du groupe ne doit pas depasser 50 caracteres.")
	@NotEmpty(message = "Le nom du groupe doit avoir une valeur.")
	@NotNull(message = "Le nom du groupe ne peut pas etre null.")
	@Column(name="nom_groupe", nullable=false )
	private String nomGroupe;	
	
	@OneToMany( cascade=CascadeType.ALL ,  mappedBy="groupe")
	private Set<Personne> personnesDuGroup = new HashSet<Personne>();
	
	// GETTERS ET SETTERS
	// Id
	public Integer getId() {
		return  id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	// nomGroupe
	public String getNomGroupe() {
		return nomGroupe;
	}
	public void setNomGroupe(String nomGroupe) {
		this.nomGroupe = nomGroupe;
	}
	
	public void addPersonneToGroup( Personne personne) {
		personne.setGroupe(this);
		personnesDuGroup.add(personne);
	}
	public Set<Personne> getPersonnesDuGroupe() {
		return personnesDuGroup;
	}
	
	@Override
	public String toString() {
		return id + " " + nomGroupe;
	}
}
