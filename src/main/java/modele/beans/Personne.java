package modele.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

/**
 * Classe gestion des Personnes
 * @author Fanny
 *
 */
@Entity
@Table(name = "personne")
public class Personne {
	
	// ATTRIBUTS D'INSTANCE
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_personne")
	private Integer id ;


	@Length(min = 10 , message = "La longueur du nom est de 10 caracteres minimum.")
	@Length(max = 25 , message = "La longueur du nom ne doit pas depasser 25 caracteres.")
	@NotEmpty(message = "Le nom doit avoir une valeur.")
	@NotNull(message = "Le nom ne peut pas etre null.")
	@Column(name="nom", nullable=false )
	private String nom;
	

	@Length(min = 10 , message = "La longueur du prenom est de 10 caracteres minimum.")
	@Length(max = 25 , message = "La longueur du prenom ne doit pas depasser 25 caracteres.")
	@NotEmpty(message = "Le prenom doit avoir une valeur.")
	@NotNull(message = "Le prenom ne peut pas etre null.")
	@Column(name="prenom", nullable=false )
	private String prenom;	
	
	@ManyToOne
	@JoinColumn(name="id_groupe")
	private Groupe groupe;
	

	// GETTERS ET SETTERS
	// Id
	public Integer getId() {
		return  id;
	}
	@SuppressWarnings("unused")
	private void setId(Integer id) {
		this.id = id;
	}
	// Nom
	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) throws Exception {
		this.nom = nom;
	}
	// Prenom
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) throws Exception {
		this.prenom = prenom;
	}
	// Groupe
	public Groupe getGroupe() {
		return groupe;
	}
	public void setGroupe(Groupe groupe) {
		this.groupe = groupe;
	}
	
	@Override
	public String toString() {
		return id + " " + nom + " " + prenom;
	}
}
