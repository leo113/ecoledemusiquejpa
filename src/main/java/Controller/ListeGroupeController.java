package Controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modele.beans.Groupe;
import modele.dao.DaoGroupe;

/**
 * Classe gérant la liste des groupe
 * @author Fanny
 */
public class ListeGroupeController implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		// INCREMENTATION DU NOMBRE DE PAGES VISITEES
		HttpSession session = request.getSession(); 		
	 	int nbrPages =  (int) session.getAttribute("compteurPage");
	 	nbrPages = nbrPages + 1;
	 	session.setAttribute("compteurPage", nbrPages); 

		
		// RECUPERATION DE L'INSTANCE DE LA LISTE D'ADHERENTS GLOBALE,
		//  CREER DANS LA METHODE INIT() DU FRONT-CONTROLLEUR.
		// PUIS, ENVOIT A LA JSP.
	 	ArrayList<Groupe> listeGroupes = new DaoGroupe().findAll();
	 	request.setAttribute("listeGroupes", listeGroupes);	
		return "listeGroupe.jsp";
	}
}
