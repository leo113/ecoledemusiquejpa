package Controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class PageAccueilController implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {	
	

		// INCREMENTATION DU NOMBRE DE PAGES VISITEES
		HttpSession session = request.getSession(); 	
 		int nbrPages =  (int) session.getAttribute("compteurPage");
	 	nbrPages = nbrPages + 1;
		session.setAttribute("compteurPage", nbrPages); 

		if (request.getParameter("init") != null) {
			session.invalidate();
		}
		
		Cookie[] cokies = request.getCookies();
		for ( int i = 0; i < cokies.length ; i++) {
			Cookie cookie = cokies[i];
			if (cookie.getName().equalsIgnoreCase("JSESSIONID")) {
				cokies[i] = null; 
			}
		}
		request.setAttribute("cokies", cokies);
		
		return "accueil.jsp";
	}
}
