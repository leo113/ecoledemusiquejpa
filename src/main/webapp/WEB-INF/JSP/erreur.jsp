<%@include file="taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>	
	<meta charset="UTF-8">
	<title>Erreur</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/united/bootstrap.min.css" media="all"/>
</head>
<body>
	<div class="container">	
		<h2>Ceci est une erreur !</h2>		
		<%@ include file="footer.jsp" %>
	</div>
</body>
</html>